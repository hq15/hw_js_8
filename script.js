/*
Обработчик событий это функция, которая срабатывает при определенном событии, то есть сигнале браузера о том что что-то произошло. Обработчик отслеживает (слушает) эти события, и благодарю этому мы можем создать какое то действие или порядок действий, создав инстрцукцию (функцию), которое (которые) будут запущены при получении сигнала браузера о происшедшем, отслеживаемом нами событии
*/

const inputPrice = document.querySelector("input");
const parentSpan = document.querySelector(".wrapper");

inputPrice.addEventListener("focus", function () {
  inputPrice.value = null;
  if (document.querySelector(".js-warning") != null) {
    document.querySelector(".js-warning").remove();
  }
  inputPrice.classList.remove("js-invalidInput");
  inputPrice.classList.add("js-input");
});

inputPrice.addEventListener("blur", function () {
  if (
    inputPrice.value < 0 ||
    inputPrice.value == "" ||
    isNaN(Number(inputPrice.value))
  ) {
    parentWarning = document.querySelector(".inputSpan");
    createAndPrependElement(
      "afterend",
      "p",
      "js-warning",
      "Please enter correct price",
      "",
      parentWarning,
      "br",
      "remove",
      ""
    );
    document.querySelector("br").remove();
    inputPrice.classList.add("js-invalidInput");
  } else {
    inputPrice.classList.remove("js-invalidInput");
    //this.classList.add("input-done");

    createAndPrependElement(
      "afterBegin",
      "span",
      "js-span",
      "Текущая цена: ",
      this.value,
      parentSpan,
      "button",
      "js-remove",
      "X"
    );
    document.querySelector(".js-remove").addEventListener("click", function () {
      this.parentNode.remove();
      inputPrice.value = null;
    });
  }
  inputPrice.classList.remove("js-input");
});

function createAndPrependElement(
  position,
  tagName,
  classNames,
  textContent,
  value,
  parentElement,
  tagNameRemove,
  classNameRemove,
  textContentRemove
) {
  parentElement.insertAdjacentHTML(
    `${position}`,
    `<${tagName} class=${classNames}> ${textContent} ${value}<${tagNameRemove} class=${classNameRemove}> ${textContentRemove} </${tagNameRemove}>`
  );
}
